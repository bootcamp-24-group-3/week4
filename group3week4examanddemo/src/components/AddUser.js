import React, {Fragment, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Students from './Students';
import {v4 as uuid} from 'uuid';
import {useNavigate} from 'react-router-dom';
import css from './Home.module.css'  ;
import Styles from './AddUser.css';
import {Navbar, Container} from 'react-bootstrap';



export const AddUser = () => {

  const [firstName, setFirstName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [yearLevel, setYearLevel] = useState('');
  const [section, setSection] = useState('');
  const [city, setCity] = useState('');
  const [contactNumber, setContactNumber] = useState('');
  const [email, setEmail] = useState('');
  const [zipCode, setZipCode] = useState('');

  let history = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    const ids = uuid();
    let uniqueId = ids.slice(0,8);

    let userFirstName = firstName;
    let userMiddleName = middleName;
    let userLastName = lastName;
    let userAge = age;
    let userYearLevel = yearLevel;
    let userSection = section;
    let userCity = city;
    let userContactNumber = contactNumber;
    let userEmail = email;
    let userZipCode = zipCode;
    
    Students.push({
        id: uniqueId,
        firstName: userFirstName,
        middleName: userMiddleName,
        lastName: userLastName,
        age: userAge,
        yearLevel: userYearLevel,
        section: userSection,
        city: userCity,
        contactNumber: userContactNumber,
        email: userEmail,
        zipCode: userZipCode
    });

    history('/');

  }

  return (
<Fragment>
    <Navbar bg="dark" variant="dark" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/" className={css.brand}>Student Information System</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                </Navbar.Collapse>
            </Container>
    </Navbar>
  <body>
      <div id='main'>
        <div id='addHead'>
          Add Student
        </div>
      <Form >
        <div id='input'>
        <Form.Group  className='mb-3' controlId='formFirstName'>
          <label>Name</label>
          <Form.Control id='firstSec' type='text' placeholder='' required onChange={(e) => setFirstName(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Form.Group  className='mb-3' controlId='formMiddleName'>
        <label>Middle Name</label>
          <Form.Control id='firstSec' type='text' placeholder='' required onChange={(e) => setMiddleName(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Form.Group id='fisrtSec' className='mb-3' controlId='formLastName'>
        <label>Last Name</label>
          <Form.Control id='firstSec' type='text' placeholder='' required onChange={(e) => setLastName(e.target.value)}>
          </Form.Control>
        </Form.Group>
        </div>

        <div id='input'>
        <Form.Group className='mb-3' controlId='formAge'>
        <label>Age</label>
          <Form.Control id='secondSec' type='text' placeholder='' required onChange={(e) => setAge(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Form.Group className='mb-3' controlId='formYearLevel'>
        <label>Year Level</label>
          <Form.Control id='secondSec' as='select'  required onChange={(e) => setYearLevel(e.target.value)}>
            <option value='' disabled selected ></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </Form.Control>
        </Form.Group>
        <Form.Group  className='mb-3' controlId='formSection'>
        <label>Section</label>
          <Form.Control id='secondSec' as='select' required onChange={(e) => setSection(e.target.value)}>
            <option value='' disabled selected ></option>
            <option value="Saint Francis">Saint Francis</option>
            <option value="Saint Paul">Saint Paul</option>
            <option value="Saint Therese">Saint Therese</option>
            <option value="Saint John">Saint John</option>
            <option value="Saint Peter">Saint Peter</option>
            <option value="Saint Luke">Saint Luke</option>
          </Form.Control>
        </Form.Group>
        </div>

        <div id='input'>
        <Form.Group className='mb-3' controlId='formCity'>
        <label>City</label>
          <Form.Control id='city' type='text' placeholder='' required onChange={(e) => setCity(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Form.Group className='mb-3' controlId='formZipCode'>
        <label>Zip Code</label>
          <Form.Control id='zip' type='text' placeholder='' required onChange={(e) => setZipCode(e.target.value)}>
          </Form.Control>
        </Form.Group>
        </div>

        <div id='input'>
        <Form.Group className='mb-3' controlId='formContactNumber'>
        <label>Contact Number</label>
          <Form.Control id='fourthSec' type='text' placeholder='' required onChange={(e) => setContactNumber(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <Form.Group className='mb-3' controlId='formEmail'>
        <label>Email Address</label>
          <Form.Control id='fourthSec' type='email' placeholder='' required onChange={(e) => setEmail(e.target.value)}>
          </Form.Control>
        </Form.Group>
        <div id='addBtnCont'>
        <Button id='addNowBtn' className='btn btn-success' onClick={(e) => handleSubmit(e)} type='submit'>Add Now</Button> 
        <Button id='cancelBtn' className='btn btn-warning' type='button' href='/'>Cancel</Button>    
       
        </div>
        </div>

      </Form>
      </div>
    
    
  </body>
  </Fragment>
  )
}