import React, { Fragment, useState } from 'react'
import {Button, Modal, Navbar, Nav, Container, Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Students from './Students';
import {Link, useNavigate} from 'react-router-dom';
import css from './Home.module.css';

export const Home = () => {

    let history = useNavigate();

    const [data, setData] = useState(Students);
    const [show, setShow] = useState(false);
    const [studentSelected, setStudentSelected] = useState('');
    const [order, setOrder] = useState('ASC');
    const [identification, setIdentification] = useState(0);
    const [selectedSort, setSelectedSort] = useState();
    const [selectedSection, setSelectedSection] = useState('');
    const [selectedYearLevel, setSelectedYearLevel] = useState('');
    const [selectedMinAge, setselectedMinAge] = useState('');
    const [selectedMaxAge, setselectedMaxAge] = useState('');
    const [search, setSearch] = useState('');

    const handleDelete = (id) => {
        
        let index = Students.map(function(e) {
            return e.id;
        }).indexOf(id);

        Students.splice(index, 1);
        history('/');
        setShow(false);
        setData(Students);
    }

    const handleClose = () => {
        setShow(false);
    }

    const handleShow = (name, id) => {
        setShow(true);
        setStudentSelected(name);
        setIdentification(id);
    }

    //sorting by column
    const sorting = (col) => {
        if(order === 'ASC') {
            const sorted = [...data].sort((a, b) =>
                a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
            );
            setData(sorted);
            setOrder('DSC');
        }
        if(order === 'DSC') {
            const sorted = [...data].sort((a, b) =>
                a[col].toLowerCase() < b[col].toLowerCase() ? 1 : -1
            );
            setData(sorted);
            setOrder('ASC');
        }
    };

    //Sorting by Section and Yearlvl
    const handleGrouping = (section, yearLevel) => {
        const group = data.filter((student) => {
            return student.yearLevel === yearLevel && student.section === section;
        });
        setData(group);
    }

    const handleAgeGrouping = (minAge, maxAge) => {
        const group = data.filter((student) => {
            return student.age >= minAge && student.age <= maxAge;
        });
        setData(group);
    }

    const handleEdit =( id, firstName
        ,middleName
        ,lastName
        ,age
        ,yearLevel
        ,section
        ,city
        ,contactNumber
        ,email
        ,zipCode) =>{

            localStorage.setItem('firstName', firstName);
            localStorage.setItem('middleName', middleName);
            localStorage.setItem('lastName', lastName);
            localStorage.setItem('age', age);
            localStorage.setItem('yearLevel', yearLevel);
            localStorage.setItem('section', section);
            localStorage.setItem('city', city);
            localStorage.setItem('contactNumber', contactNumber);
            localStorage.setItem('email', email);
            localStorage.setItem('zipCode', zipCode);
            localStorage.setItem('id', id);

    }

    const handleSearchStudent = (search) => {
        const searchHere = data.filter((student) => {
            return student.firstName === search || student.lastName === search || student.middleName === search;
        })
        setData(searchHere);
    }

  return (
    <Fragment>
        <Navbar bg="dark" variant="dark" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/">Student Information System</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                <Nav
                    className="me-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                >
                    

                </Nav>
                <Form className="d-flex">
                    <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={e=>setSearch(e.target.value)}
                    />
                    <Button onClick={() => handleSearchStudent(search)} variant="outline-success">Search</Button>
                </Form>

                </Navbar.Collapse>
            </Container>
        </Navbar>
        <div className={css.mainContainer} >
            
            <div className={css.tableContainer}>
                <h3 className='section-header'>Student List</h3>
                <table>
                    <thead align="center">
                        <tr id={css.header}>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>Age</th>
                            <th>Year Level</th>
                            <th>Section</th>
                            <th>City</th>
                            <th>Contact Number</th>
                            <th>Email</th>
                            <th>Zip Code</th>
                            <th colSpan={2}>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data && data.length > 0 ?  data.map((student) => {
                                return(
                                    <tr>
                                        <td>{student.firstName}</td>
                                        <td>{student.middleName}</td>
                                        <td>{student.lastName}</td>
                                        <td>{student.age}</td>
                                        <td>{student.yearLevel}</td>
                                        <td>{student.section}</td>
                                        <td>{student.city}</td>
                                        <td>{student.contactNumber}</td>
                                        <td>{student.email}</td>
                                        <td>{student.zipCode}</td>
                                        <td>
                                            <Link to={'/edit'}>
                                                <Button onClick={() => handleEdit(student.id
                                                    ,student.firstName
                                                    ,student.middleName
                                                    ,student.lastName
                                                    ,student.age
                                                    ,student.yearLevel
                                                    ,student.section
                                                    ,student.city
                                                    ,student.contactNumber
                                                    ,student.email
                                                    ,student.zipCode
                                    
                                                    )}>Edit</Button>
                                            </Link>
                                            
                                        </td>
                                        <td>
                                            <Button data-toggle='modal' onClick={() => handleShow(student.firstName, student.id)}>Delete</Button>
                                            
                                            <Modal show={show} onHide={handleClose}>
                                                <Modal.Header>Deleting Student Information</Modal.Header>
                                                <Modal.Body>
                                                    You are deleting {studentSelected}'s Information. Are you sure you want to do this?
                                                </Modal.Body>
                                                <Modal.Footer>
                                                    <Button variant='secondary' onClick={handleClose}>Cancel</Button>
                                                    <Button variant="primary" onClick={() => handleDelete(identification)  }>Yes</Button>
                                                </Modal.Footer>
                                            </Modal>

                                        </td>
                                    </tr>
                                )
                            })
                            : "No data found"
                        }
                    </tbody>
                </table>
            </div>
           <div className={css.commands}>
                <div className={css.addButton}>
                    
                    <Link to="/create">
                        <Button className='btn btn-success' >Add New Student</Button>
                    </Link>
                </div>
                
                <h3 className='section-header'>Sorting Commands</h3>
                <div className={css.commandDivision}>
                    <div className={css.sortCommand}>
                        <label for='commandDiv'>Sorting Section</label>
                        <select class="form-select" aria-label="Default select example" id='groupBy' value={selectedSort} onChange={e=>setSelectedSort(e.target.value)}>
                            <option selected>Sort by</option>
                            <option value="firstName">First Name</option>
                            <option value="middleName">Middle Name</option>
                            <option value="lastName">Last Name</option>
                            <option value="age">Age</option>
                            <option value="yearLevel">Year Level</option>
                            <option value="section">Section</option>
                            <option value="city">City</option>
                            <option value="cocntactNumber">Contact Number</option>
                            <option value="email">Email</option>
                            <option value="zipCode">Zip Code</option>
                        </select>
                        <Button className='btn' onClick={() => sorting(selectedSort)}>Sort Now</Button>
                    </div>

                    <div className={css.groupBySectionAndYearLevel}>
                        <label for='group'>Grouping Section</label>
                        <div id='group' className={css.groupBySectionAndYearLevelDivision}>
                        
                            <div className={css.groupSection}>
                                
                                <select class="form-select" aria-label="Default select example" id='groupBy' value={selectedSection} onChange={e=>setSelectedSection(e.target.value)}>
                                    <option selected>Section</option>
                                    <option value="Saint Francis">Saint Francis</option>
                                    <option value="Saint Paul">Saint Paul</option>
                                    <option value="Saint Therese">Saint Therese</option>
                                    <option value="Saint John">Saint John</option>
                                    <option value="Saint Peter">Saint Peter</option>
                                    <option value="Saint Luke">Saint Luke</option>
                                </select>
                            </div>
                            <div className={css.groupYearLevel}>
                                <select class="form-select" aria-label="Default select example" id='groupBy' value={selectedYearLevel} onChange={e=>setSelectedYearLevel(e.target.value)}>
                                    <option selected>Year Level</option>
                                    <option value="1">1st Year</option>
                                    <option value="2">2nd Year</option>
                                    <option value="3">3rd Year</option>
                                    <option value="4">4th Year</option>
                                </select>
                            </div>
                        </div>
                        <Button className='btn' onClick={() => handleGrouping(selectedSection, selectedYearLevel)}>Group Now</Button>
                      
                    </div>
                        
                    <div className={css.groupBySectionAndYearLevel}>
                        <label for='ageGroup'>Age Bracket</label>
                        <div id='ageGroup' className={css.groupBySectionAndYearLevelDivision}>
                        
                            <div className={css.groupSection}>
                                <label for="quantity">Min:</label>
                                <input  class="form-select" aria-label="Default select example" id='groupBy' type="number" name="quantity" value={selectedMinAge} onChange={e=>setselectedMinAge(e.target.value)}></input>
                            </div>
                            <div className={css.groupYearLevel}>
                                <label for="quantity">Max:</label>
                                <input  class="form-select" aria-label="Default select example" id='groupBy' type="number" name="quantity" value={selectedMaxAge} onChange={e=>setselectedMaxAge(e.target.value)}></input>
                            </div>
                        </div>
                        <Button className='btn' onClick={() => handleAgeGrouping(selectedMinAge, selectedMaxAge)}>Sort Now</Button>
                      
                    </div>
               </div>
               
                
           </div>
        </div>
    </Fragment>
  )
}
